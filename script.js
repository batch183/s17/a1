function printHello(){
console.log("Hello World");
}


let students = []

function addStudent(newEntry){
	students.push(newEntry);
	console.log(newEntry + " was added to the student's list.");
}

function countStudents() {
	console.log("There are a total of " + students.length + " students enrolled.")
}

function printStudents(){
	students.sort();
	students.forEach(function(student){
		console.log(student);
	})
}

function findStudents(search){
	let filteredStudents = students.filter(function(student){
		return student.toLowerCase().includes(search);
	});

	if (filteredStudents.length>1) {
	console.log(filteredStudents + " are enrollees.");
	}
	else if (filteredStudents.length == 1) {
	console.log(filteredStudents + " is an enrollee.");
	}
	else {
	console.log(search + " is not an enrollee.");	
	}
}

function addSection(sectionName){
	let studentSection = students.map(function(student){
		return student + " - section " +sectionName;
	})
	console.log(studentSection)
}

function removeStudent(removeName) {
	let end = removeName.slice(1,removeName.length);
	let start = removeName.toUpperCase().slice(0,1);
	let indexWord = start + end;
	let locateStudent = students.indexOf(indexWord);
	let removedStudent = students.splice(locateStudent,1);
	console.log(removeName + " was removed from the student's list.");

}
